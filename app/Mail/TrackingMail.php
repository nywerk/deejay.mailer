<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TrackingMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public string $name,
        public string $trackingCode,
        public string $mailTemplate
    ) {
        //
    }

    public function build()
    {
        return $this->from('tracking@deejay.de', 'Deejay.de')
            ->subject('New Tracking-Status')
            ->markdown('mail.tracking.' . $this->mailTemplate)->with([
                'name' => $this->name,
                'email' => $this->trackingCode,
            ]);
    }
}
