<?php

namespace App\Jobs;

use App\Mail\TrackingMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendTrackingMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        private string $trackingCode,
        private string $mailTemplate,
    ) {
        //
    }

    public function handle()
    {
        // TODO: Load customer data from service
        Mail::to('test@test.de')->send(new TrackingMail(
            'Name Empfänger',
            $this->trackingCode,
            $this->mailTemplate,
        ));
    }
}
