<?php

namespace App\Http\Controllers;

use App\Jobs\SendTrackingMail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TrackingMailController extends Controller
{
    public function __invoke(Request $request): Response
    {
        SendTrackingMail::dispatch(
            $request->trackingCode,
            $request->mailTemplate,
        );

        return response([
            'success' => true,
        ]);
    }
}
