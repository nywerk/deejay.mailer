<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendTrackingMail;
use App\Mail\TrackingMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class TriggerTrackingMailJobTest extends TestCase
{
    use RefreshDatabase;

    public function test_trigger_in_transit_mail()
    {
        Mail::fake();
        SendTrackingMail::dispatch(
            '1ZR80W966894419084',
            'in-transit'
        );
        Mail::assertSent(TrackingMail::class);
    }
}
