@component('mail::message')
# Out for Delivery

Hallo {{$name}},

Paketnummer {{$trackingCode}}

@component('mail::button', ['url' => 'https://liefertool.de'])
    Hier live verfolgen
@endcomponent

Solltest Du Fragen haben erreichst Du uns unter info@deejay.de
@endcomponent
